import {API_HOLIDAY_ALT_LIST} from "./routeConstants";

const headers = new Headers({
  // 'Content-Type': 'application/json'
});

export const fetchHolidaysCall = (country, year) => {
  const method = 'GET';
  const url = API_HOLIDAY_ALT_LIST.replace(':year', year).replace(':country', country);
  const request = new Request(
    url,
    {
      method,
      headers
    });

  return fetch(request).then(response => {
    return response.json().then(body => ({ status: response.status, body }))
  })
}
