import {fetchHolidaysCall} from "./calls";
import {GET_HOLIDAY_LIST, LOADING_TRUE} from "./actionTypes";

export const fetchHolidays = (country = 'US', year = 2020) => dispatch => {
  dispatch({ type: LOADING_TRUE });
  return fetchHolidaysCall(country, year).then(response => {
    switch (response.status) {
      case 200:
        dispatch({type: GET_HOLIDAY_LIST, body: response.body});
        break;
      default:

    }
  });
}
