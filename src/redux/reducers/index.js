import { combineReducers } from "redux";
import holiday from "./holiday";

export default combineReducers({ holiday });

