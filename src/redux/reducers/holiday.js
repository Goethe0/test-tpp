import {DELETE_HOLIDAY, GET_HOLIDAY_LIST, LOADING_FALSE, LOADING_TRUE} from "../actionTypes";

const initialState = {
  loading: false,
  list: {
    holidays: []
  },
};

export default function holiday(state = initialState, action) {
  switch (action.type) {
    case LOADING_TRUE:
      return { ...state, loading: true };
    case LOADING_FALSE:
      return { ...state, loading: false };
    case GET_HOLIDAY_LIST:
      return { ...state, list: action.body, loading: false };
    case DELETE_HOLIDAY:
      return {
        ...state
      }
    default:
      return state;
  }
}
