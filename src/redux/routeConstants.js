export const API_HOLIDAY_LIST = 'https://date.nager.at/api/v2/PublicHolidays/:year/:country';
export const API_HOLIDAY_ALT_LIST = 'https://holidayapi.com/v1/holidays?pretty&key=5d772730-5afb-4fcc-ba25-a3edf72b0811&country=:country&year=:year';
