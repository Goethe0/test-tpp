import React from "react";
import { makeStyles, Button, AppBar, Menu, MenuItem, Typography, Toolbar, IconButton } from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    // flexGrow: 1,
  },
  menuTitle: {
    color: '#FFF'
  }
}));


const HolidayHeader = ({changeCountry}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (country) => {
    setAnchorEl(null);
    changeCountry(country);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Total Party Planner
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} className={classes.menuTitle}>
              Holidays
            </Button>
          </Typography>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={() => handleClose('US')}>United States</MenuItem>
            <MenuItem onClick={() => handleClose('CA')}>Canada</MenuItem>
            <MenuItem onClick={() => handleClose('AR')}>Argentina</MenuItem>
            <MenuItem onClick={() => handleClose('BR')}>Brazil</MenuItem>
            <MenuItem onClick={() => handleClose('MX')}>Mexico</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default HolidayHeader;
