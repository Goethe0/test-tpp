import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import HolidayHeader from "./HolidayHeader";
import HolidayBody from "./HolidayBody";
import { fetchHolidays } from "../redux/actions";

function HolidayPage(props) {
  const { fetchHolidays, holiday: { list, loading } } = props;
  const [rows, setRows] = useState([]);

  useEffect(() => {
    fetchHolidays();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setRows(list.holidays);
  }, [list])

  const deleteRow = (item) => {
    setRows(rows.filter(row => row.name !== item.name));
  }

  const changeCountry = (country) => {
    fetchHolidays(country);
  }

  return (
    <>
      <HolidayHeader changeCountry={changeCountry} />
      <HolidayBody rows={rows} loading={loading} deleteRow={deleteRow}/>
    </>
  );
}

const mapStateToProps = state => {
  return { holiday: state.holiday };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchHolidays
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HolidayPage);
