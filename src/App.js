import React from "react";
import './App.css';
import HolidayPage from "./components/HolidayPage";


function App() {
  return (
    <div className="App">
      <HolidayPage />
    </div>
  );
}

export default App;
